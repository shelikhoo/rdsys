module gitlab.torproject.org/tpo/anti-censorship/rdsys

go 1.14

require (
	github.com/NullHypothesis/zoossh v0.0.0-20211012143359-017a7be2e713
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/emersion/go-imap v1.2.0
	github.com/emersion/go-imap-idle v0.0.0-20210907174914-db2568431445
	github.com/emersion/go-sasl v0.0.0-20211008083017-0b9dcfb154ac // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-github v17.0.0+incompatible
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/prometheus/client_golang v1.11.0
	github.com/prometheus/common v0.31.1 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	golang.org/x/net v0.0.0-20211011170408-caeb26a5c8c0 // indirect
	golang.org/x/oauth2 v0.0.0-20211005180243-6b3c2da341f1
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
